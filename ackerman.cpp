#include<iostream>

using namespace std;

int A(int m,int n);//functon ackerman

int main(){

    int m=0;
    int n=0;

    while(!cin.eof()){//loop to get input until EOF
        cin>>m>>n;
        cout<<A(m,n)<<endl;
    }

    return 0;
}

int A(int m,int n){

    if(m==0)return n+1;
    if(n==0) return A(m-1,1);
    else return A(m-1,A(m,n-1));
    
}