#include<iostream>
#include<string>

using namespace std;

void powerset(string *str,string* now,int tot,int num,int count);

int main(){
    
    
    
    while(!cin.eof()){

        int num=0;
        string s="";
        string now[10]={""};
        string a[10]={""};

        getline(cin,s); //get the input line by line

        for(int i=0;i<s.length();i++){
            //save the inputs as array
            if(s[i]!=' '){
                a[num]+=s[i];
            }

            //drop the space
            else{
                num++;
            }
        }
        powerset(a,now,num,0,0);
    }

    return 0;
}

void powerset(string *str,string* now,int tot,int num,int count){
    
    //print out current string
    cout<<"{";
    for(int i=0;i<=num;i++){
        cout<<now[i];
        if(i!=num)cout<<" ";
    }
    cout<<"}";

    //when the counter equal to totoal numbers of the element , return
    if(count==tot+1){
        return;
    }
    

    for(int i=count,j=count;i<=tot;i++){
        now[j]=str[i];  //add new element into current array 
        powerset(str,now,tot,num+1,count+1);
        now[j]="";
        count++;
    }

    return;
}
